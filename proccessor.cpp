#include "proccessor.h"
#include <iostream>
#include <iomanip>
using namespace std;
void printMatrix(QVector<QVector<double>> m) {
    const int SPAN = 10;
    cout << setw(12) << " ";
    for(int i = 0; i < m[0].size(); ++i)
        cout << setw(SPAN) << i+1;
    cout << endl;
    for(int i = 0; i < 80; ++i)
        cout << "-";
    cout << endl;
    for(int i = 0; i < m.size();++i) {
        cout << setw(SPAN) <<  i + 1 << " |";
        for(double v : m[i])
            cout << setw(SPAN) << v;
        cout << endl;
    }
    cout << endl;
}

Proccessor::Proccessor(BarSystem t)
{
    task = t;
}

Proccessor::~Proccessor()
{

}

double Proccessor::K(int p) {
    Bar& b = task.bar(p);
    return (b.A * b.elasticity) / task.length(p);
}

double Proccessor::Q(int p) {
    Bar &b = task.bar(p);
    return b.q * task.length(p) * 0.5;
}

void Proccessor::fillA() {
    int N = task._joint.size();
    A.resize(N);
    for(auto &v: A) {
        v.resize(N);
    }
    for(int i = 0; i < A.size(); ++i) {
        if(task.joint(i).fixedEnd)
            A[i][i] = 1;
        else {
            if( i == 0) {
                A[i][i] = K(i);
                A[i][i+1] = -K(i);
                continue;
            }
            else if(i == N-1) {
                A[i][i] = K(i-1);
                A[i][i-1] = -K(i-1);
                break;
            }
            else {
                A[i][i] = K(i-1) + K(i);
                A[i][i+1] = -K(i);
                A[i][i-1] = -K(i-1);
            }
        }
    }
    printMatrix(A);
}

void Proccessor::fillB() {
    int N = task._joint.size();
    B.resize(N);
    for(int i = 0; i < N; ++i) {
        if(task.joint(i).fixedEnd)
            B[i] = 0;
        else if (i ==0)
            B[i] = Q(i) + task.joint(i).F;
        else if(i == N-1)
            B[i] = Q(i-1) + task.joint(i).F;
        else
            B[i] = Q(i-1) + Q(i) + task.joint(i).F;
    }
}

QVector<double> Proccessor::gaussSolution() {
    const int N = A.size();
    QVector<QVector<double>> m(N);
    for(int i  = 0;i<A.size();++i) {
        m[i] = A[i];
        m[i].push_back(B[i]);
    }
    printMatrix(m);

    for (int i=0; i<N; i++) {
        // Search for maximum in this column
        double maxEl = abs(m[i][i]);
        int maxRow = i;
        for (int k=i+1; k<N; k++) {
            if (abs(m[k][i]) > maxEl) {
                maxEl = abs(m[k][i]);
                maxRow = k;
            }
        }

        // Swap maximum row with current row (column by column)
        for (int k=i; k<N+1;k++) {
            double tmp = m[maxRow][k];
            m[maxRow][k] = m[i][k];
            m[i][k] = tmp;
        }

        // Make all rows below this one 0 in current column
        for (int k=i+1; k<N; k++) {
            double c = -m[k][i]/m[i][i];
            for (int j=i; j<N+1; j++) {
                if (i==j) {
                    m[k][j] = 0;
                } else {
                    m[k][j] += c * m[i][j];
                }
            }
        }
    }

    // Solve equation Ax=b for an upper triangular matrix A
    QVector<double> x(N);
    for (int i=N-1; i>=0; i--) {
        x[i] = m[i][N]/m[i][i];
        for (int k=i-1;k>=0; k--) {
            m[k][N] -= m[k][i] * x[i];
        }
    }
    //        for(auto v : m) {
    //            for(auto d: v)
    //                std::cout << d << "\t";
    //            std::cout << "\n";
    //        }
    return x;
}

ComputationResult Proccessor::proccess() {
    fillA();
    fillB();
    QVector<double> delta = gaussSolution();
    cout << "delta = ";
    for(double v : delta)
        cout << v << " ";
    cout << endl;
    ComputationResult r(task,delta);
    for(int i = 0; i < task._bar.size(); ++i) {
        cout << i+1 << ". 0: " <<  r.N(i,0) << " L: " << r.N(i,task.length(i)) << endl;
    }
    return r;
}

//int main() {
//    typedef Joint J;
//    typedef BarInput B;
//    typedef BarType T;
//    BarSystem bs(
//    {J(0,true,0), J(1,false,0),J(3,false,1),J(5,false,0),J(6,true,0)},
//    {B(1,1,0), B(2,0,0),B(2,0,0),B(1,-1,0)},
//    {T(1,1)});
//    Proccessor proc(bs);
//    proc.proccess();
//}
