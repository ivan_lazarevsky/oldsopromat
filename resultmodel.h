#ifndef RESULTMODEL_H
#define RESULTMODEL_H
#include "computationresult.h"

struct Entry {
    double u0;
    double ul;
    double N0;
    double Nl;
    double sigma0;
    double sigmal;

    Entry() {}
    Entry (double u0,double ul,double N0,double Nl, double sigma0,double sigmal){
         this->u0 =  u0; this-> ul = ul;this->Nl = Nl;
        this->N0 = N0; this->sigma0 = sigma0; this->sigmal = sigmal;
    }
};

class ResultModel : public QAbstractListModel
{
    Q_OBJECT
public:
    ResultModel();
    Q_INVOKABLE int rowCount(const QModelIndex &parent) const;
    Q_INVOKABLE int columnCount(const QModelIndex &parent) const;
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role) const;
    void fill(ComputationResult& res);
    enum {
        IndexRole = Qt::UserRole+1,
        U0Role, ULRole, N0Role, NLRole, Sigma0Role,SigmaLRole
    };
    QHash<int,QByteArray> roleNames() const;
    QComputationResult result;
    QBarSystem system;
    ~ResultModel();
private:

    QVector<Entry> entries;
    QHash<int,QByteArray> _roles = {
        {IndexRole, "index"}, {U0Role, "u0"}, {ULRole, "ul" },
        {N0Role, "N0"}, {NLRole, "Nl"}, {Sigma0Role,"sigma0"}, {SigmaLRole, "sigmal"}
    };
};

#endif // RESULTMODEL_H
