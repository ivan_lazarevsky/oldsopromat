#include "barsystem.h"
#include "utils.h"
BarSystem::BarSystem()
{}

Joint::Joint(double x, bool end, double f)
    : x(x), fixedEnd(end), F(f) {}

BarType::BarType(double e, double l)
    : elasticity(e), lim(l) {}

BarInput::BarInput(double A, double q, int type)
    : A(A), q(q), type(type) {}

Bar::Bar(double A, double q, double e, double lim)
    : A(A), q(q), elasticity(e), lim(lim) {}


BarSystem::BarSystem(QVector<Joint> j, QVector<BarInput> b, QVector<BarType> t) {
    // Что делать с пустым вводом?
    r_assert(j.size() - b.size() == 1 && j.size()>=2 , "Некорректное число глобальных узлов и стержней");
    for(BarInput &bar : b) {
       // qDebug() << (bar.type >= 0) << (bar.type < t.size()) << (bar.type >= 0 && bar.type < t.size());
        r_assert(bar.type >= 0 && bar.type < t.size(),"Некорректный номер типа стержня");
    }
    _joint = j;
    for(BarInput &bar : b) {
        int i = bar.type;
        _bar.push_back(Bar(bar.A,bar.q,t[i].elasticity,t[i].lim));
    }
}

Bar& BarSystem::bar(int i){
    return _bar[i];
}
Joint& BarSystem::joint(int i) {
    return _joint[i];
}

Joint BarSystem::j0(int bar){
    return _joint[bar];
}
Joint BarSystem::jL(int bar) {
    return _joint[bar+1];
}

double BarSystem::length(int barIndex) {
    return jL(barIndex).x - j0(barIndex).x;
}

int BarSystem::barCount() {
    return _bar.size();
}

int BarSystem::jointCount() {
    return _joint.size();
}




BarSystem::~BarSystem()
{

}

