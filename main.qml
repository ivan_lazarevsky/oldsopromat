

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import my.components 1.0
ApplicationWindow {
    id: appWindowId
    title: qsTr("Hello World")
    width: 640
    height: 700
    visible: true
    CppSignalHandler {
        id: handlerId
        objectName: "mainHandler"
        onErrorRecieved: {
            inputObserver.text = message;
            inputObserver.visible = true;
        }
    }

    Rectangle  {
        id: bgId
        anchors.fill: parent
        property int topPanelHeight: 50
        MessageDialog {
            id: inputObserver
            title: "Неправильные исходные данные"
            visible: false
        }
        Rectangle {
            id: topPanel
            width: parent.width
            anchors.top: parent.top
            height: bgId.topPanelHeight
            color: "#ddd"
            Button {
                property Window results:  ResultWindow {
                    id: resultsTableId
                }
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 15
                text: "Рассчитать"
                onClicked: {
                    if(driver.launchSolution())
                        resultsTableId.visible = true;
                }
            }
        }

        ScrollView {
            anchors {
                top: parent.top;
                topMargin:bgId.topPanelHeight
                left: parent.left
                right: parent.right
                bottom: parent.bottom

            }
            Rectangle {
                id: inputBackgroundId
                width: bgId.width
                height: bgId.height;
                color: "#ddd"
                GridLayout {
                    columns: 2
                    anchors {
                        left: parent.left
                        right: parent.right
                        top: parent.top
                        margins: 10
                    }
                    width: parent.width

                    id: inputGrid

                    property double tableWidth: jointRowId.width
                    TableView {
                        id: jointTableId
                        //   Layout.minimumWidth: 300
                        Layout.minimumHeight: 150
                        Layout.maximumWidth: inputGrid.tableWidth
                        Layout.minimumWidth: inputGrid.tableWidth
                        model: jointModel
                        TableViewColumn { role: "index"; title: "#"; width: jointTableId.width/4.0  }
                        TableViewColumn { role: "x"; title: "x"; width: jointTableId.width / 4.0}
                        TableViewColumn { role: "s"; title: "Опора"; width: jointTableId.width / 4.0 }
                        TableViewColumn { role: "F"; title: "F"; width: jointTableId.width / 4.0 }
                        function getProperty(role) {
                            if(currentRow >= 0)
                                return jointModel.get(currentRow,role);
                            else return "";
                        }
                    }
                    ControlButtons {
                        id: jointControls
                        Layout.fillWidth: false
                        modify.onClicked: {
                            var row = jointTableId.currentRow;
                            if(row >= 0) {
                                var xText = jointXField.text, support = (jointSupportField.box.currentIndex == 1),
                                        fText = jointLoadField.text;
                                if(xText != "" && fText != "") {
                                    jointModel.modify(row,"s",support);
                                    jointModel.modify(row,"F",parseFloat(fText));
                                    jointModel.modify(row,"x",parseFloat(xText)); // Злая шутка
                                }
                            }
                        }
                        add.onClicked: {
                            var xText = jointXField.text, support = (jointSupportField.box.currentIndex == 1),
                                    fText = jointLoadField.text;
                            if(xText != "" && fText != "") {
                                jointModel.append(parseFloat(xText),support,parseFloat(fText));
                            }
                        }
                        remove.onClicked: {
                            var row = jointTableId.currentRow;
                            if(row >= 0) {
                                jointModel.deleteRow(row);
                            }
                        }
                    }

                    Row {
                        id: jointRowId
                        spacing: 5
                        Item {
                            width: 25
                            height: 10
                        }
                        VerticalField {
                            id: jointXField
                            label.text: "x"
                            Layout.fillWidth: false
                            field.validator: DoubleValidator {
                                locale:"us_US";
                            }
                            text: jointTableId.getProperty("x")
                        }
                        Column {
                            id: jointSupportField
                            property alias box: jointbox
                            spacing: jointXField.spacing
                            Layout.fillWidth: false
                            Text {

                                anchors.horizontalCenter: parent.horizontalCenter
                                text: "Опора"
                            }
                            ComboBox {
                                width: jointXField.width
                                id: jointbox
                                model: [ "Нет", "Да"]
                                height: jointXField.field.height
                                currentIndex: (jointTableId.getProperty("s") === true) ? 1 : 0
                            }

                        }
                        VerticalField {
                            id: jointLoadField
                            label.text: "F"
                            Layout.fillWidth: false
                            field.validator: DoubleValidator {
                                locale:"us_US";
                            }
                            text: jointTableId.getProperty("F")
                        }
                        Item {
                            width: 25
                            height: 10
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    TableView {
                        id: barTableId
                        Layout.minimumHeight: 150
                        Layout.maximumHeight: 150

                        Layout.minimumWidth: inputGrid.tableWidth
                        Layout.maximumWidth: inputGrid.tableWidth
                        model: barModel
                        onCurrentRowChanged: { if(currentRow >= 0) {selection.clear(); selection.select(currentRow);} }
                        TableViewColumn { role: "index"; title: "Узел 1"; width: barTableId.width / 4.0 }
                        TableViewColumn { role: "A"; title: "A";  width: barTableId.width / 4.0}
                        TableViewColumn { role: "q"; title: "q";  width: barTableId.width / 4.0}
                        TableViewColumn { role: "type"; title: "Тип"; width: barTableId.width / 4.0 }
                        function getProperty(role) {
                            if(currentRow >= 0)
                                return barModel.get(currentRow,role);
                            return "";
                        }
                    }
                    Column {
                        spacing: barControls.spacing
                        Button {
                            id: upButton
                            text: "△"
                            onClicked: {
                                barTableId.currentRow = barModel.moveUp(barTableId.currentRow);
                            }
                        }

                        ControlButtons {
                            id: barControls
                            Layout.fillWidth: false
                            modify.onClicked: {
                                var row = barTableId.currentRow;
                                if(barTableId.currentRow >=0 ) {
                                    var aText = barAField.text, qText = barQField.text, tText = barTypeField.text;
                                    if(aText != "" && qText != "" && tText != "")
                                    {
                                        barModel.modify(row,"A",parseFloat(aText));
                                        barModel.modify(row,"q",parseFloat(qText));
                                        barModel.modify(row,"type",parseInt(tText));
                                    }
                                }
                            }
                            add.onClicked: {
                                var aText = barAField.text, qText = barQField.text, tText = barTypeField.text;
                                if(aText != "" && qText != "" && tText != "")
                                    barModel.append(parseFloat(aText),parseFloat(qText),parseInt(tText));
                            }
                            remove.onClicked: {
                                if(barTableId.currentRow >= 0) {
                                    barModel.deleteRow(barTableId.currentRow);
                                }
                            }
                        }

                        Button {
                            id: downButton
                            text: "▽"
                            onClicked: {
                                barTableId.currentRow = barModel.moveDown(barTableId.currentRow);
                            }
                        }
                    }
                    Row {
                        id: barRowId
                        spacing: 5
                        Item {
                            height: barAField.height
                            width: 25
                        }
                        VerticalField {
                            id: barAField
                            label.text: "A"
                            Layout.fillWidth: false
                            field.validator: DoubleValidator {
                                locale:"us_US";
                                bottom: 0
                            }
                            text: barTableId.getProperty("A");
                        }
                        VerticalField {
                            id: barQField
                            label.text: "q"
                            Layout.fillWidth: false
                            field.validator: DoubleValidator {
                                locale:"us_US";
                            }
                            text: barTableId.getProperty("q");
                        }
                        VerticalField {
                            id: barTypeField
                            label.text: "Тип"
                            Layout.fillWidth: false
                            field.validator: IntValidator {
                                locale:"us_US";
                                bottom: 0
                            }
                            text: barTableId.getProperty("type");
                        }
                        Item {
                            height: barAField.height
                            width: 25
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                    }

                    TableView {
                        id: typeTableId
                        Layout.minimumHeight: 150
                        Layout.maximumHeight: 150

                        Layout.minimumWidth: inputGrid.tableWidth
                        Layout.maximumWidth: inputGrid.tableWidth
                        model: typeModel
                        TableViewColumn { role: "index"; title: "#"; width: barTableId.width / 4.0 }
                        TableViewColumn { role: "E"; title: "E";  width: barTableId.width / 4.0}
                        TableViewColumn { role: "lim"; title: "[σ]";  width: barTableId.width / 4.0}
                        function getProperty(role) {
                            if(currentRow >= 0)
                                return typeModel.get(currentRow,role);
                            else return ""
                        }
                    }
                    ControlButtons {
                        id: typeControls
                        Layout.fillWidth: false
                        modify.onClicked:  {
                            var row = typeTableId.currentRow;
                            if(row >= 0) {
                                var eText = typeEField.text, sigmaText = typeLimField.text;
                                if(eText!= "" && sigmaText != "")
                                {
                                    typeModel.modify(row,"E",parseFloat(eText));
                                    typeModel.modify(row,"lim",parseFloat(sigmaText));
                                }
                            }
                        }
                        add.onClicked: {
                            var eText = typeEField.text, sigmaText = typeLimField.text;
                            if(eText!= "" && sigmaText != "") {
                                typeModel.append(parseFloat(eText), parseFloat(sigmaText));
                            }
                        }
                        remove.onClicked: {
                            if(typeTableId.currentRow >= 0)
                                typeModel.deleteRow(typeTableId.currentRow);
                        }
                    }
                    Row {
                        id: typeRowId
                        spacing: 5
                        Item {
                            height: barAField.height
                            width: (inputGrid.tableWidth - 3*parent.spacing - typeEField.width - typeLimField.width)/2
                        }
                        VerticalField {
                            id: typeEField
                            label.text: "E"
                            Layout.fillWidth: false
                            field.validator: DoubleValidator {
                                locale:"us_US";
                                bottom: 0
                            }
                            text: typeTableId.getProperty("E")
                        }
                        VerticalField {
                            id: typeLimField
                            label.text: "[σ]"
                            Layout.fillWidth: false
                            field.validator: DoubleValidator {
                                locale:"us_US";
                            }
                            text: typeTableId.getProperty("lim")
                        }
                        Item {
                            height: barAField.height
                            width: (inputGrid.tableWidth - 3*parent.spacing - typeEField.width - typeLimField.width)/2
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                    }

                }

            }
        }
    }
}

