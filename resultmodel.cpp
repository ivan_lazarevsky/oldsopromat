#include "resultmodel.h"

ResultModel::ResultModel()
{

}

ResultModel::~ResultModel()
{

}
int ResultModel::rowCount(const QModelIndex &parent) const {
    (void)parent;
    return entries.size();
}
int ResultModel::columnCount(const QModelIndex &parent) const {
    (void)parent;
    return 7;
}

QHash<int,QByteArray> ResultModel::roleNames() const {
    return _roles;
}

QVariant ResultModel::data(const QModelIndex &index, int role) const {
    if(!index.isValid()) return QVariant();
    Entry e = entries.at(index.row());
    switch(role) {
    case IndexRole:
        return index.row();
    case U0Role:
        return e.u0;
    case ULRole:
        return e.ul;
    case N0Role:
        return e.N0;
    case NLRole:
        return e.Nl;
    case Sigma0Role:
        return e.sigma0;
    case SigmaLRole:
        return e.sigmal;
    }
    return QVariant();
}

void ResultModel::fill(ComputationResult &res) {
    if(entries.size() >0) {
        beginRemoveRows(QModelIndex(),0,entries.size() - 1);
        entries.clear();
        endRemoveRows();
    }
    system.setAttributes(res.source);
    result.setAttributes(res);
    int size = res.barCount();
    beginInsertRows(QModelIndex(),0,size-1);
    entries.resize(size);
    BarSystem &bs = res.source;
    for(int i = 0; i < size; ++i) {
        entries[i]= Entry(res.U0(i),res.UL(i),res.N(i,0),
                          res.N(i,bs.length(i)), res.sigma(i,0), res.sigma(i,bs.length(i)));
    }
    endInsertRows();
}
