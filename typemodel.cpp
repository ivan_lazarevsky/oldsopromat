#include "typemodel.h"
#include "utils.h"
TypeModel::TypeModel()
{

}

TypeModel::~TypeModel()
{

}


QVector<BarType> TypeModel::summary() {
    return _data;
}

QHash<int,QByteArray> TypeModel::roleNames() const {
    return _roles;
}

int TypeModel::columnCount(const QModelIndex &parent) const {
    (void)parent;
    return 3;
}

int TypeModel::rowCount(const QModelIndex &parent) const {
    (void)parent;
    return _data.size();
}
QVariant TypeModel::get(int row, QString role) {
    int numRole = (_roles_reverted.contains(role)) ? _roles_reverted[role] : -1;
    return data(this->index(row),numRole);
}
bool TypeModel::deleteRow(int row) {
    if(row >= 0 && row < _data.size()) {
        int sz = _data.size() - 1;
        beginRemoveRows(QModelIndex(),row,row);
        _data.remove(row);
        endRemoveRows();
        my_assert(sz == _data.size(), "sz == _data.size()");
        emit dataChanged(this->index(0),this->index(rowCount()-1));
        return true;
    }
    return false;
}

QVariant TypeModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    BarType b = _data.at(index.row());
    switch(role) {
    case IndexRole:
        return index.row();
    case ERole:
        return b.elasticity;
    case LimRole:
        return b.lim;
    default:
        return QVariant();
    }
}

bool TypeModel::modify(int row, const QString &roleName, const QVariant &value) {
    if(row >= 0 && row < _data.size()) {
        my_assert(_roles_reverted.contains(roleName),"Invalid role name");
        int role = _roles_reverted[roleName];
        BarType &b = _data[row];
        bool ok = false;
        switch(role) {
        case ERole:
        {
            double E = value.toDouble(&ok);
            my_assert(ok,"Wrong type to bar elasticity");
            b.elasticity = E;
            break;
        }
        case LimRole: {
            double lim = value.toDouble(&ok);
            my_assert(ok,"Wrong type to bar lim");
            b.lim = lim;
            break;
        }
        default:
            my_assert(false, "WTF? Index is modified");
        }
        emit dataChanged(this->index(row),this->index(row));
        return true;
    }
    return false;
}

bool TypeModel::append(QVariant E,QVariant lim) {
    bool ok = false;
    double _E = E.toDouble(&ok);
    my_assert(ok,"Wrong type to bar elasticity");
    double _lim = lim.toDouble(&ok);
    my_assert(ok,"Wrong type to bar lim");
    BarType b(_E,_lim);
    beginInsertRows(QModelIndex(),rowCount(),rowCount());
    _data.push_back(b);
    endInsertRows();
    return true;
}
