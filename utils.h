#ifndef UTILS_H
#define UTILS_H
#include <QtCore>

class BasicException {
    QString m_what;
public:
    BasicException(QString msg);
    QString what();
};

class SAPException : public BasicException {
public:
    SAPException(QString msg);
};

class InvalidInputException: public BasicException {
public:
    InvalidInputException(QString msg);
};

class InvalidUserInput: public QObject {
    Q_OBJECT
public:
    void newError(QString msg);
    virtual ~InvalidUserInput();
signals:
    void errorRecieved(QString msg);
};
extern InvalidUserInput inHandler;

void my_assert(bool pred,QString msg);
void r_assert(bool pred, QString msg);
void track(bool pred,QString msg);

#endif // UTILS_H
