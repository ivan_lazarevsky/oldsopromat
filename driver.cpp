#include "driver.h"
#include "proccessor.h"
Driver::Driver()
{
}

Driver::~Driver()
{

}

bool Driver::launchSolution() {
    try {
        BarSystem bs(jointModel.summary(),barModel.summary(),typeModel.summary());
        Proccessor p(bs);
        ComputationResult res = p.proccess();
        resultModel.fill(res);
        (void)bs;
    }
    catch(BasicException& ex) {
        return false;
    }
    return true;
}

void Driver::testConfiguration(Driver& d) {

        d.jointModel.append(0,true,0);
        d.jointModel.append(1,false,0);
        d.jointModel.append(3,false,1);
        d.jointModel.append(5,false,0);
        d.jointModel.append(6,true,0);
        d.barModel.append(1,1,0);
        d.barModel.append(2,0,0);
        d.barModel.append(2,0,0);
        d.barModel.append(1,-1,0);
        d.typeModel.append(1,1);
        //    {J(0,true,0), J(1,false,0),J(3,false,1),J(5,false,0),J(6,true,0)},
        //    {B(1,1,0), B(2,0,0),B(2,0,0),B(1,-1,0)},
        //    {T(1,1)});

}
