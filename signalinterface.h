#ifndef SIGNALINTERFACE_H
#define SIGNALINTERFACE_H
#include <QObject>

class SignalInterface : public QObject
{
    Q_OBJECT
public slots:
    void newError(QString message);
public:
    virtual ~SignalInterface();
signals:
    void errorRecieved(QString message);
};

#endif // SIGNALINTERFACE_H
