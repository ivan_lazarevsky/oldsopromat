#ifndef PROCCESSOR_H
#define PROCCESSOR_H
#include "barsystem.h"
#include "utils.h"
#include "computationresult.h"
class Proccessor
{
    BarSystem task;
    QVector<QVector<double>> A;
    QVector<double> B;
    void fillA();
    void fillB();
    double K(int bar);
    double Q(int bar);
public:
    // ComputationResult proccess();
    ComputationResult proccess();

    QVector<double> gaussSolution();
    Proccessor(BarSystem t);
    ~Proccessor();
};

#endif // PROCCESSOR_H
