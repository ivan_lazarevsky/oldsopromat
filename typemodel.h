#ifndef TYPEMODEL_H
#define TYPEMODEL_H
#include <QtCore>
#include "barsystem.h"

class TypeModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum { IndexRole = Qt::UserRole +1, LimRole, ERole };
    TypeModel();
    Q_INVOKABLE int columnCount(const QModelIndex &parent) const;
    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Q_INVOKABLE bool deleteRow(int row);
    Q_INVOKABLE bool append(QVariant E, QVariant lim);
    Q_INVOKABLE QVariant get(int row, QString role);
    QVariant data(const QModelIndex &index, int role) const;
    Q_INVOKABLE bool modify(int row, const QString& roleName, const QVariant& value);
    QVector<BarType> summary();
    QHash<int,QByteArray> roleNames() const;
    ~TypeModel();
private:
    QVector<BarType> _data;
    QHash<int,QByteArray> _roles = {
      {IndexRole,"index"}, {ERole,"E"}, {LimRole, "lim"}
    };
    QHash<QString,int> _roles_reverted = {
        {"index", IndexRole}, {"E", ERole}, {"lim", LimRole}
    };
};

#endif // TYPEMODEL_H
