#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>
#include "driver.h"
#include "utils.h"
#include "signalinterface.h"

int main(int argc, char *argv[])
{
    (void)argc; (void)argv;
    Driver driver;
    driver.testConfiguration(driver);

    QApplication app(argc, argv);
    QQmlApplicationEngine engine;
    qmlRegisterType<SignalInterface>("my.components",1,0,"CppSignalHandler");
    engine.rootContext()->setContextProperty("driver",&driver);
    engine.rootContext()->setContextProperty("jointModel", &driver.jointModel);
    engine.rootContext()->setContextProperty("barModel",&driver.barModel);
    engine.rootContext()->setContextProperty("typeModel",&driver.typeModel);
    engine.rootContext()->setContextProperty("resultModel", &driver.resultModel);
    engine.rootContext()->setContextProperty("sourceSystem",&driver.resultModel.system);
    engine.rootContext()->setContextProperty("changesSystem",&driver.resultModel.result);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    QObject *rootWindow = engine.rootObjects()[0];
    SignalInterface* handler = dynamic_cast<SignalInterface*>(rootWindow->findChild<QObject*>("mainHandler"));
    QObject::connect(&inHandler,&InvalidUserInput::errorRecieved,handler,&SignalInterface::newError);
    return app.exec();

}
