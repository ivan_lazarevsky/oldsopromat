#ifndef BARMODEL_H
#define BARMODEL_H
#include "barsystem.h"

class BarModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum { AreaRole = Qt::UserRole+1, IndexRole, QRole, TypeRole };
    BarModel();
    Q_INVOKABLE int columnCount(const QModelIndex &parent) const;
    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Q_INVOKABLE bool deleteRow(int row);
    Q_INVOKABLE bool append(QVariant A, QVariant q, QVariant type);
    Q_INVOKABLE QVariant get(int row, QString role);
    Q_INVOKABLE int moveUp(int row);
    Q_INVOKABLE int moveDown(int row);
    //Q_INVOKABLE QVariant getProperty(int row, QString roleName);
    QVariant data(const QModelIndex &index, int role) const;
    Q_INVOKABLE bool modify(int row, const QString& roleName, const QVariant& value);
    QVector<BarInput> summary();
    QHash<int,QByteArray> roleNames() const;
    ~BarModel();
private:
    QVector<BarInput> _data;
    QHash<int,QByteArray> _roles = {
        {IndexRole,"index"},{AreaRole,  "A"}, {QRole, "q"}, {TypeRole,"type"}
    };
    QHash<QString,int> _roles_reverted = {
        {"index",IndexRole},{"A", AreaRole}, {"q", QRole}, {"type", TypeRole}
    };
};

#endif // BARMODEL_H
