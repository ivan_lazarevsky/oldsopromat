#ifndef COMPUTATIONRESULT_H
#define COMPUTATIONRESULT_H
#include "barsystem.h"

class ComputationResult
{
public:
    QVector<double> _U0;
    QVector<double> _UL;

    int barCount();
    BarSystem source;
    virtual double U0(int p);
    virtual double UL(int p);
    virtual double u(int p, double x);
    virtual double N(int p, double x);
    virtual double sigma(int p, double x);
    ComputationResult(BarSystem bs, QVector<double> delta);
    ComputationResult() {}
    ~ComputationResult();
};

class QComputationResult : public QObject, public ComputationResult
{
    Q_OBJECT
public:
    void setAttributes(ComputationResult &r) {
        source = r.source;
        _U0 = r._U0;
        _UL = r._UL;
    }
    Q_INVOKABLE double U0(int p) { return ComputationResult::U0(p); }
    Q_INVOKABLE double UL(int p) { return ComputationResult::UL(p); }
    Q_INVOKABLE double u(int p, double x) { return ComputationResult::u(p,x); }
    Q_INVOKABLE double N(int p, double x)  { return ComputationResult::N(p,x); }
    Q_INVOKABLE double sigma(int p, double x)  { return ComputationResult::sigma(p,x); }
    QComputationResult(ComputationResult &r) : ComputationResult(r) {}
    QComputationResult() {}

};


#endif // COMPUTATIONRESULT_H
