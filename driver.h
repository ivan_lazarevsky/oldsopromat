#ifndef DRIVER_H
#define DRIVER_H
#include "utils.h"
#include "barsystem.h"
#include "barmodel.h"
#include "jointmodel.h"
#include "typemodel.h"
#include "resultmodel.h"
class Driver : public QObject
{
    Q_OBJECT
public:
    JointModel jointModel;
    BarModel barModel;
    TypeModel typeModel;
    ResultModel resultModel;
    Q_INVOKABLE bool launchSolution();
    Driver();
    ~Driver();
    static void testConfiguration(Driver &d);
};

#endif // DRIVER_H
