#ifndef JOINTMODEL_H
#define JOINTMODEL_H
#include <QtCore>
#include "barsystem.h"
class JointModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum JointRoles {
        IndexRole = Qt::UserRole+1,
        XRole,
        SupportRole,
        LoadRole
    };
    JointModel();
    Q_INVOKABLE int columnCount(const QModelIndex &parent) const;
    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Q_INVOKABLE bool deleteRow(int row);
    Q_INVOKABLE bool append(QVariant x, QVariant end, QVariant f);
    Q_INVOKABLE QVariant get(int row, QString role);
    //Q_INVOKABLE QVariant getProperty(int row, QString roleName);
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int,QByteArray>roleNames() const;
   // bool setData(const QModelIndex &index, const QVariant &value, int role); //Может нафиг ее?
    Q_INVOKABLE bool modify(int row, const QString& roleName, const QVariant& value);
    void sort();
    QVector<Joint> summary();
    ~JointModel();
private:
    QVector<Joint> _data;
    QHash<int,QByteArray> _roles = {
        {IndexRole, "index"},
        {XRole,"x"},
        {SupportRole, "s"},
        {LoadRole, "F"}
    };
    QHash<QString,int> _roles_reverted {
        {"index", IndexRole},
        {"x", XRole},
        {"s",SupportRole },
        {"F", LoadRole }
    };
};

#endif // JOINTMODEL_H
