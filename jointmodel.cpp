#include "jointmodel.h"
#include <algorithm>
#include "utils.h"
JointModel::JointModel()
{}

JointModel::~JointModel()
{}

QVector<Joint> JointModel::summary() {
    return _data;
}

//void JointModel::sort() {
//    std::sort(_data.begin(),_data.end(),[](Joint j1, Joint j2) {
//        return j1.x < j2.x;
//    });
//    emit dataChanged(this->index(0),this->index(_data.size()));
//}
void JointModel::sort() {
    for(int j = 1; j < rowCount(); ++j) {
        Joint key = _data[j];
        int i = j - 1;
        for(;(i>= 0) && (_data[i].x > key.x); --i) {
          //  beginMoveRows(QModelIndex(),i+1,i+1,QModelIndex(),i);
            _data[i+1] = _data[i];
          //  emit dataChanged(this->index(i+1),this->index(i+1),{XRole,SupportRole,LoadRole,IndexRole});
           // endMoveRows();
        }
        _data[i+1] = key;
      //  emit dataChanged(this->index(i+1),this->index(i+1),{XRole,SupportRole,LoadRole,IndexRole});
    }
    emit dataChanged(this->index(0),this->index(rowCount()-1));
}

int JointModel::columnCount(const QModelIndex &parent) const {
    (void)parent;
    return 4;
}

int JointModel::rowCount(const QModelIndex &parent) const {
    (void)parent;
    return _data.size();
}
QHash<int,QByteArray> JointModel::roleNames() const {
    return _roles;
}

QVariant JointModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(role == IndexRole) {
        return index.row();
    }
    Joint j = _data.at(index.row());
    switch(role) {
    case XRole:
        return j.x;
    case SupportRole:
        return j.fixedEnd;
    case LoadRole:
        return j.F;
    default:
        return QVariant();
    }
}

bool JointModel::deleteRow(int row) {
    if(row >= 0 && row < _data.size()) {
        int sz = _data.size() - 1;
        beginRemoveRows(QModelIndex(),row,row);
        _data.remove(row);
        endRemoveRows();
        my_assert(sz == _data.size(), "sz == _data.size()");
        emit dataChanged(this->index(0),this->index(rowCount()-1));
        return true;
    }
    return false;
}

QVariant JointModel::get(int row, QString role) {
    int numRole = (_roles_reverted.contains(role)) ? _roles_reverted[role] : -1;
    return data(this->index(row),numRole);
}

bool JointModel::modify(int row, const QString &roleName, const QVariant &value) {
    if(row >= 0 && row < _data.size()) {
        my_assert(_roles_reverted.contains(roleName),"Invalid role name");
        int role = _roles_reverted[roleName];
        Joint& j = _data[row];
        bool ok = false;
        switch(role) {
        case XRole:
        {
            double x = value.toDouble(&ok);
            my_assert(ok,"Wrong type to joint x");
            j.x = x;
            emit dataChanged(this->index(row),this->index(row));
            sort();
            break;
        }
        case SupportRole:
            j.fixedEnd = value.toBool();
            emit dataChanged(this->index(row),this->index(row));
            break;
        case LoadRole:
        {
            double f = value.toDouble(&ok);
            my_assert(ok,"Wrong type to joint f");
            emit dataChanged(this->index(row),this->index(row));
            j.F = f;
            break;
        }
        default:
            my_assert(false, "WTF? Index is modified");
        }
        return true;
    }
    return false;
}

bool JointModel::append(QVariant x, QVariant end, QVariant f) {
    bool ok = false;
    double _x = x.toDouble(&ok);
    my_assert(ok,"Wrong type to joint x");
    bool s = end.toBool();
    double _f = f.toDouble(&ok);
    my_assert(ok,"Wrong type to joint f");
    Joint j(_x,s,_f);
    beginInsertRows(QModelIndex(),rowCount(),rowCount());
    _data.push_back(j);
    endInsertRows();
    sort();
    return true;
}


