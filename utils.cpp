#include "utils.h"

InvalidUserInput inHandler;
BasicException::BasicException(QString msg)
    : m_what(msg) {}

QString BasicException::what() {
    return m_what;
}

SAPException::SAPException(QString msg) : BasicException(msg) {}
InvalidInputException::InvalidInputException(QString msg) : BasicException(msg) {}
void InvalidUserInput::newError(QString msg) {
    emit errorRecieved(msg);
}

InvalidUserInput::~InvalidUserInput(){}

void my_assert(bool pred, QString msg) {
    static QString fail = "Assertion failed: ";
    if(!pred)
        throw SAPException(fail + msg);
}

void r_assert(bool pred, QString msg)
{
 //   qDebug() << pred;
    if(!pred) {
        inHandler.newError(msg);
        throw InvalidInputException(msg);
    }
}

void track(bool pred, QString msg) {
    QString status = (pred) ? "True" : "False";
    qDebug() << msg << "is" << status;
}
