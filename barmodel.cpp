#include "barmodel.h"
#include "utils.h"
BarModel::BarModel()
{}

BarModel::~BarModel()
{}

QVector<BarInput> BarModel::summary() {
    return _data;
}

QHash<int,QByteArray> BarModel::roleNames() const {
    return _roles;
}

int BarModel::columnCount(const QModelIndex &parent) const {
    (void)parent;
    return 4;
}

int BarModel::rowCount(const QModelIndex &parent) const {
    (void)parent;
    return _data.size();
}

int BarModel::moveUp(int row) {
    if(row > 0 && row < rowCount()) {
        BarInput temp = _data[row];
        _data[row] = _data[row-1];
        _data[row - 1] = temp;
        emit dataChanged(this->index(row-1),this->index(row));
        return row -1;
    }
    return row;
}

int BarModel::moveDown(int row) {
    if(row >= 0 && row < rowCount() - 1) {
        BarInput temp = _data[row];
        _data[row] = _data[row+1];
        _data[row + 1] = temp;
        emit dataChanged(this->index(row),this->index(row+1));
        return row+1;
    }
    return row;
}

QVariant BarModel::get(int row, QString role) {
    int numRole = (_roles_reverted.contains(role)) ? _roles_reverted[role] : -1;
    return data(this->index(row),numRole);
}
bool BarModel::deleteRow(int row) {
    if(row >= 0 && row < _data.size()) {
        int sz = _data.size() - 1;
        beginRemoveRows(QModelIndex(),row,row);
        _data.remove(row);
        endRemoveRows();
        my_assert(sz == _data.size(), "sz == _data.size()");
        emit dataChanged(this->index(0),this->index(rowCount()-1));
        return true;
    }
    return false;
}

QVariant BarModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    BarInput b = _data.at(index.row());
    switch(role) {
    case IndexRole:
        return index.row();
    case AreaRole:
        return b.A;
    case QRole:
        return b.q;
    case TypeRole:
        return b.type;
    default:
        return QVariant();
    }
}

bool BarModel::modify(int row, const QString &roleName, const QVariant &value) {
    if(row >= 0 && row < _data.size()) {
        my_assert(_roles_reverted.contains(roleName),"Invalid role name");
        int role = _roles_reverted[roleName];
        BarInput &b = _data[row];
        bool ok = false;
        switch(role) {
        case AreaRole:
        {
            double A = value.toDouble(&ok);
            my_assert(ok,"Wrong type to bar area");
            b.A = A;
            break;
        }
        case QRole: {
            double q = value.toDouble(&ok);
            my_assert(ok,"Wrong type to bar q");
            b.q = q;
            break;
        }
        case TypeRole:
        {
            int type = value.toInt(&ok);
            my_assert(ok,"Wrong type to bar type");
            b.type = type;
            break;
        }
        default:
            my_assert(false, "WTF? Index is modified");
        }
        emit dataChanged(this->index(row),this->index(row));
        return true;
    }
    return false;
}

bool BarModel::append(QVariant A, QVariant q, QVariant type) {
    bool ok = false;
    double _A = A.toDouble(&ok);
    my_assert(ok,"Wrong type to bar area");
    double _q = q.toDouble(&ok);
    my_assert(ok,"Wrong type to bar q");
    int _type = type.toInt(&ok);
    my_assert(ok,"Wrong type to bar type");
    BarInput b(_A,_q,_type);
    beginInsertRows(QModelIndex(),rowCount(),rowCount());
    _data.push_back(b);
    endInsertRows();
    return true;
}
