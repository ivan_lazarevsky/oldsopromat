#ifndef BARSYSTEM_H
#define BARSYSTEM_H
#include <QtCore>
struct Joint {
    double x;
    bool fixedEnd;
    double F;
    Joint(double x  =0 , bool end = false , double f = 0);
};
//class QJoint : public QObject, public Joint {
//    Q_OBJECT
//    Q_PROPERTY(double x MEMBER x)
//    Q_PROPERTY(double end MEMBER fixedEnd)
//    Q_PROPERTY(double F MEMBER F)
//public:
//    QJoint(Joint j) : Joint(j) {}
//};

struct Bar {
    double A;
    double q;
    double elasticity;
    double lim;
    Bar(double A = 0, double q = 0,  double e = 0, double lim = 0);
};

//class QBar : public QObject, public Bar {
//    Q_OBJECT
//    Q_PROPERTY(double A MEMBER A)
//    Q_PROPERTY(double q MEMBER q)
//    Q_PROPERTY(double E MEMBER elasticity)
//    Q_PROPERTY(double lim MEMBER lim)
//public:
//    QBar(Bar b) : Bar(b) {}
////    double getA() { return A; }
////    double getq() { return q; }
////    double getE() { return elasticity; }
////    double getLim() { return lim; }
//};

struct BarType {
    double elasticity;
    double lim;
    BarType(double e = 0, double l = 0);
};

struct BarInput {
    double A;
    double q;
    int type;
    BarInput(double A = 0, double q = 0, int type = 0);
};

// В случае необходимости транслировать Bar и BarType во что-то общее по получении данных из модели
struct BarSystem
{
    QVector<Joint> _joint;
    QVector<Bar> _bar;
    // Провести проверку ввода
public:
    BarSystem();
    BarSystem(QVector<Joint> j, QVector<BarInput> b, QVector<BarType> t);
    virtual int barCount();
    virtual int jointCount();
    virtual Joint& joint(int i);
    virtual Bar& bar(int i);
    virtual Joint j0(int bar);
    virtual Joint jL(int bar);
    virtual double length(int barIndex);
    ~BarSystem();
};
#define SUPER BarSystem::
class QBarSystem : public QObject, public BarSystem{
    Q_OBJECT
public:
    void setAttributes(BarSystem &bs) {
        _joint = bs._joint;
        _bar = bs._bar;
    }

    Q_INVOKABLE int barCount() { return SUPER barCount(); }
    Q_INVOKABLE int jointCount() { return SUPER jointCount(); }
    Q_INVOKABLE double length(int barIndex) { return SUPER length(barIndex); }
    Q_INVOKABLE double x(int j) { return joint(j).x; }
    Q_INVOKABLE bool end(int j) { return joint(j).fixedEnd; }
    Q_INVOKABLE double  F(int j) { return joint(j).F; }
    Q_INVOKABLE double A(int b) { return bar(b).A; }
    Q_INVOKABLE double E(int b) { return bar(b).elasticity; }
    Q_INVOKABLE double q(int b) { return bar(b).q; }
    Q_INVOKABLE double sigma(int b) { return bar(b).lim; }
};
#undef SUPER

#endif // BARSYSTEM_H
