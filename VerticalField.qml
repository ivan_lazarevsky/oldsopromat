import QtQuick 2.4
import QtQuick.Controls 1.3

Item {
    id: rootId
    property alias label : textId
    property alias field : editId
    property alias text : editId.text
    property int spacing : 2
    width: Math.max(textId.width, editId.width)
    height: textId.height + editId.height + 2
    Text {
        id: textId
        anchors.horizontalCenter: parent.horizontalCenter
        smooth: true
    }
    TextField {
        width: Math.max(100,textId.width)
        height: implicitHeight
        anchors.top: textId.bottom
        anchors.topMargin: spacing
        id: editId
        anchors.horizontalCenter: parent.horizontalCenter
    }
//    onSpacingChanged: editId.anchors.topMargin  = spacing
//    Component.onCompleted: {
//        spacingChanged()
//    }
    onWidthChanged: {
        editId.width=  width
    }
}
