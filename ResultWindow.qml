import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2

Window {
    height: 600
    width: 600

    Rectangle {
        anchors.fill: parent
        color: "#edc"
        TabView {
            anchors.fill: parent

            Tab {
                title: "Таблица"
                Rectangle {
                    anchors.fill: parent
                    color: "#bcb"
                    TableView {
                        id: resultView
                        model: resultModel
                        anchors.fill: parent
                        anchors.margins: 20
                        TableViewColumn { role: "index"; title: "Стержень"; width: resultView.width/7}
                        TableViewColumn { role: "u0"; title: "U0"; width: resultView.width/7}
                        TableViewColumn { role: "ul"; title: "UL"; width: resultView.width/7}
                        TableViewColumn { role: "N0"; title: "N0"; width: resultView.width/7}
                        TableViewColumn { role: "Nl"; title: "NL"; width: resultView.width/7}
                        TableViewColumn { role: "sigma0"; title: "σ0"; width: resultView.width/7}
                        TableViewColumn { role: "sigmal"; title: "σL"; width: resultView.width/7}
                    }
                }
            }

            Tab {
                id: drawTab
                title: "Рисунок"
                Rectangle {
                    id: drawTabBg

                    anchors.fill: parent
                    color: "white"
                    Canvas {
                        id: canvasBarId
                        anchors.fill: parent
                        anchors.margins: 50
                        property double hscale: 1
                        property double vscale: hscale/2
                        property var src: sourceSystem
                        property var mid: height/2
                        property var context;
                        function drawRect(x,y,width,height) {
                            console.log(x + " " + y)
                            context.moveTo(x,y);
                            context.lineTo(x+width,y);
                            context.lineTo(x+width,y-height);
                            context.lineTo(x,y-height)
                            context.lineTo(x,y);
                            context.stroke();
                        }

                        function drawBar(p) {
                            drawRect(src.x(p)*hscale, mid + src.A(p)/2 * vscale,
                                     src.length(p)*hscale,src.A(p)*vscale);
                        }

                        onPaint: {

                            context = getContext("2d")
                            context.beginPath();
                            context.clearRect(0, 0, width, height);
                            context.fill();
                            print("painting bar")
                            var barc = src.barCount();
                            var maxLength = src.x(barc) - src.x(0);
                            hscale = width* 1.0 / maxLength
                            for(var i = 0; i < barc; ++i)
                                drawBar(i);
                        }
                        onVisibleChanged: {
                            if(visible)
                            requestPaint();
                        }
                    }
                }
            }
            Tab {
                title: "Графики"
                Rectangle {
                    color: "#eee"
                    Rectangle {

                        height: 40
                        border.width: 2
                        width: parent.width
                        ComboBox {
                            id: chooseChart
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: 20
                            model: [ "u(x)", "N(x)","σ(x)" ]
                            onCurrentIndexChanged: chartCanvas.requestPaint()
                        }
                    }
                    Canvas {
                        id: chartCanvas
                        height: parent.height - 40
                        width: parent.width
                        property var src: sourceSystem
                        property var hscale: 0.8 * width / (src.x(src.barCount()) - src.x(0))
                        property var mid: height/2
                        property var vscale: 1
                        onPaint: {
                            var fun = changesSystem.u, idx = chooseChart.currentIndex;
                            if(idx == 0)
                                fun = changesSystem.u;
                            else if(idx == 1)
                                fun = changesSystem.N;
                            else fun = changesSystem.sigma;
                            var barc = src.barCount();
                            var minV= 0;
                            var maxV = 0;
                            for(var i = 0; i < barc; ++i) {
                                var fun0 = fun(i,0);
                                var funl = fun(i,src.length(i));
                                if(fun0 < minV)
                                    minV = fun0;
                                if(funl < minV)
                                    minV = funl;
                                if(fun0 > maxV)
                                    maxV = fun0;
                                if(funl > maxV)
                                    maxV = funl;
                            }
                            vscale = 0.2 * height / (Math.max(1,maxV+minV))
                            console.log(vscale + " " + hscale)
                            var context = getContext("2d");
                            context.beginPath();
                                    context.clearRect(0, 0, width, height);
                                    context.fill();
                            context.moveTo(0,mid)

                            context.lineTo(width,mid);
                            context.strokeStyle = "red"
                            context.stroke()


                            var c = 0.1 * width;
                            for(var i = 0; i < barc; ++i) {
                                var fun0 = fun(i,0);
                                var funl = fun(i,src.length(i));
                                var x0 = (c + src.x(i) * hscale);
                                var y0 = mid - fun0*vscale
                                context.beginPath();
                                context.fillStyle = "orange"
                                context.strokeStyle = "black"
                                context.moveTo(x0,y0);
                                context.strokeText(fun0,x0,y0 + ((y0 > mid) ? 20 : -20))
                                var xl = c + src.x(i+1)*hscale;
                                var yl = mid - funl*vscale
                                context.lineTo(xl,yl)
                                context.strokeText(funl,xl,yl + ((yl > mid) ? 20 : -20))
                                context.lineTo(xl,mid)
                                context.lineTo(x0,mid)
                                context.lineTo(x0,y0);
                                context.stroke()
                                context.fill()


                            }


                        }
                    }
                }
            }
        }
    }
}
