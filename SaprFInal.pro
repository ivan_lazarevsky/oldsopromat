TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    barsystem.cpp \
    utils.cpp \
    jointmodel.cpp \
    barmodel.cpp \
    typemodel.cpp \
    driver.cpp \
    proccessor.cpp \
    computationresult.cpp \
    signalinterface.cpp \
    resultmodel.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    barsystem.h \
    utils.h \
    jointmodel.h \
    barmodel.h \
    typemodel.h \
    driver.h \
    proccessor.h \
    computationresult.h \
    signalinterface.h \
    resultmodel.h

QMAKE_CXXFLAGS += -std=c++11
