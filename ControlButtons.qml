import QtQuick 2.4
import QtQuick.Controls 1.3

Column {
    spacing: 5
    property alias modify: modifyId
    property alias add: addId
    property alias remove: removeId
    Button {
        id: modifyId
        text: "Изменить"
    }
    Button {
        id: addId
        text: "Добавить"
    }
    Button {
        id: removeId
        text: "Удалить"
    }
}
