#include "computationresult.h"

ComputationResult::ComputationResult(BarSystem bs,QVector<double> delta)
{
    source = bs;
    _U0.resize(source._bar.size());
    _UL.resize(_U0.size());
    for(int i = 0; i < source._bar.size(); ++i) {
        _U0[i] = delta[i];
        _UL[i] = delta[i+1];
    }
}

int ComputationResult::barCount() {
    return _UL.size();
}

double ComputationResult::U0(int p) {
    return _U0.at(p);
}

double ComputationResult::UL(int p) {
    return _UL.at(p);
}

ComputationResult::~ComputationResult()
{

}

double ComputationResult::u(int p, double x) {
    Bar bp = source.bar(p);
    double Lp = source.length(p);
    return U0(p) + (UL(p) - U0(p)) * x / Lp + (1 - x/Lp) * bp.q * Lp * Lp * x / ( 2*bp.elasticity * bp.A * Lp);
}

double ComputationResult::N(int p, double x) {
    Bar bp = source.bar(p);
    double Lp = source.length(p);
    return (UL(p)-U0(p))* bp.elasticity * bp.A / Lp  + bp.q * Lp * (1 - 2*x/Lp) / 2;
}

double ComputationResult::sigma(int p, double x) {
    return N(p,x) / source.bar(p).A;
}
